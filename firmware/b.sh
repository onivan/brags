avr-gcc -g -Os -mmcu=atmega8 -c main.c
avr-gcc -g -mmcu=atmega8 -o main.elf main.o
avr-objdump -h -S main.elf > main.lst
avr-gcc -g -mmcu=atmega8 -Wl,-Map,main.map -o main.elf main.o
avr-objcopy -j .text -j .data -O ihex main.elf main.hex