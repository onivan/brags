#ifndef DISPLAY_REG_H_INCLUDED
#define DISPLAY_REG_H_INCLUDED

// Регістри дисплеїв
unsigned char D_LEDS = 0; // ЛЕДи команд
#define L_G1    0 //1 // зелений
#define L_G2    1 //2
#define L_G3    2 //4
#define L_G4    3 //8
#define L_R1    4 //16 // червоний
#define L_R2    5 //32
#define L_R3    6 //64
#define L_R4    7 //128
// Червоний = (зелений << 4) або (зелений + 4)

unsigned char D_SECONDS = 0; // секундомір
#define D_S0    0
#define D_S1    1
#define D_S2    2
#define D_S3    3
#define D_S4    4
#define D_S5    5
#define D_S6    6
#define D_S7    7


#endif // DISPLAY_REG_H_INCLUDED
