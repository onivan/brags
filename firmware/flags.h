#ifndef FLAGS_H_INCLUDED
#define FLAGS_H_INCLUDED

// ��������� ��������
//
unsigned char FGAME  = 0b00000000; //����� ����� ���
#define G_NEW_GAME          0//1
#define G_NEW_ROUND         1//2
#define G_ANSWER            2//4
#define G_CONTINUE_ROUND    3//8   2^x=8
#define G_FALSESTART        4//16   2^x=16
#define G_AFTER_TABLE_BUTT  5//32//���� � ������ ���������
#define G_ROUND   6//64 // ���������� ��� ������� ������� ������
#define G_WAIT              7//128 ���������� �����
// sum = 63

unsigned char F_INPUT = 0b00000000; // ������ ����������� �� ������
#define FI_TABLES_BUT   0 // �������� ������ �� ����� ������
#define FI_CONTROL_BUT  1 // �������� ������ �� ����� ��������� ����
#define FI_EDIT_BUT     2 // �������� ������ �� ����� �����������
#define FI_PAUSE_BUT    3
#define ENABLE_TABLE_CHECK 4 // 16


unsigned char FBUTT_TABLES = 0b00000000;// ���� ������ ������
#define T1_SW 0 // 1
#define T2_SW 1 // 2
#define T3_SW 2 // 4
#define T4_SW 3 // 8


// sum = 15

unsigned char F_NACTIVE_TABLES = 0b00000000;// ������� �������
#define F_NAT1 0 // 1
#define F_NAT2 1 // 2
#define F_NAT3 2 // 4
#define F_NAT4 3 // 8
// sum = 15

unsigned char FBUTT_CONTROL = 0b00000000; // ������ ���������
#define C_NEW_ROUND         0
#define C_ANSWER            1
#define C_CONTINUE_ROUND    2
#define C_PAUSE             3

unsigned char FBUTT_EDIT = 0b00000000;  // ������ �����������
#define EP1_SW  0
#define EP2_SW  1
#define EP3_SW  2
#define EP4_SW  3
#define E_PLUS  4
#define E_MINUS 5

unsigned char F_UPDATE_DISPLAY = 0; //������� ������ ������
#define UPD_L   0 //1 //������� ������ LED��
#define UPD_S   1 //2 //������� ������ ����������
#define UPD_C   2 //4 //������� ������ ������� ���������
#define UPD_G   3 //4 //������� ������ ������� ����� ���

unsigned char FTIMER = 0; //����� �������
#define TI_SECUNDS      0
#define TI_END_EDIT     1
#define TI_DISPL_BLICK  2
#define TI_PLAY_BIP_NOTE 3
#define TI_SHOW_GAME_MODE 4



#endif // FLAGS_H_INCLUDED
