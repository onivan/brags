/*

���� �����-������� http://windflower.spb.ru/ke/common/reglaments/brainlaws.html
2012-08-18 15:10
����� 0.0.6
+ ����� �������� ����������, ���� ������� ��������� ������ ��� ������� 1 ������� ���� ������� ������
* ��� ������ ������� �� 61 �������, ��� ���� ��������� � 60

2012-02-22-18.40
����� 0.0.6
+ ���� ����


����� 0.0.5
2012-01-27-13.35
�� ������:
+ 4 ������ ������
+ 4 ������� �� 4 �������� LED ������.
+ ���� ������ ������ � �������, �� ����� ���������, �������� ������, ���� ��������
+ ����������� ���������, � �������� ����
    1. ��������� �-�� � ����� "³������"
        ��� ����� ��������� ��������� � ������� ������ � ������ ������
    2. ���� ������� ������� ����, �� ���������� "����� �����"
    3. ���� -- ������, �� -- "���������� �����".
        ��� ����� ������� ������ � ������ ������
+ �������� ���������� � ������ ����� �������� �������� � ������ ���������

������ ����������� �����.
 - ���� �������� ������ "����� �����" ���� ���������� � 60
 - ���� �������� ������ ������, ���� �� ��������� ������ �����������
 - ���� �������� ������ "���������� �����" ������ ����� ����� ����� ����.


TODO

��������� ��� ������ ������!!!

���������� ��� �� ��������� ������ ��������

������� ���� �� 4 ���������� ��������� ��� ����������� ������
������� �������  ����������� �� ����������


*/

#define F_CPU 8000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "timer.h"
#include "flags.h"
#include "display_reg.h"

#define REG_ST_CP PORTC2 //���� ��� "������������" ������ � ������� ��������
#define REG_DS PORTC1 //���� ��� ������ ������
#define REG_SH_CP PORTC0 //���� ��� �������� ���������

void WDT_off(void)
{
  /* reset WDT */
  wdt_reset();
  /* Write logical one to WDCE and WDE */
  WDTCR |= (1<<WDCE) | (1<<WDE);
  /* Turn off WDT */
  WDTCR = 0x00;
}


void ctc_init ( void){

 // pg. 109
// For generating a waveform output in CTC mode, the OC2 output can be set to toggle its
// logical level on each Compare Match by setting the Compare Output mode bits to toggle
// mode (COM21:0 = 1). The OC2 value will not be visible on the port pin unless the data
// direction for the pin is set to output. The waveform generated will have a maximum frequency
// of fOC2 = fclk_I/O/2 when OCR2 is set to zero (0x00). The waveform frequency is
// defined by the following equation:

  // fOCn = (fclk_I_O) / (2 * N * (1 + OCRn) )

  // OCRn  = fclk / 2 / N / FOCn  - 1

  // 440 Hz
  // N 256, OCR2 = 71
  // fOCn = 8000000 / (2 *256 * (1+71) )

  // Set prescaler to 64


   TCCR2 |= _BV(FOC2) |_BV(WGM21) | _BV(COM21) | _BV(CS22) | _BV(CS20) ;
//                                      //CTC            toggle                       256
  // Enable timer 0 overflow interrupt
  TIMSK |= _BV(OCIE2);
    //TCNT2 =0; // ��������

  sei();

}

ISR (TIMER2_COMP_vect){ // �������� ����� ��� ��������� �����

PORTB ^= (1<<PORTB0 );


}



//static always_inline
// ������� static ������� �� ����� ����-����� � �������.
// 1.5.1. ����������� � ����� ������� http://citforum.ru/programming/c/h15.shtml#151


// ������������ ����� � ����� ��� ���������

#define SHIFTREG_DDR    DDRC
#define SHIFTREG_PORT PORTC

// ������0 ��������� ���������:
#define TACT0       PORTC0  //SH_CP  //���� ��� �������� ���������
#define DAT0        PORTC1  //DS  //���� ��� ������ ������
#define LOCK0       PORTC2  //ST_CP //���� ��� "������������" ������ � ������� ��������

// ������1 ��������� ����
#define TACT1       PORTC3  //SH_CP  //���� ��� �������� ���������
#define DAT1        PORTC4  //DS  //���� ��� ������ ������
#define LOCK1       PORTC5  //ST_CP //���� ��� "������������" ������ � ������� ��������


// �������
#define SETBIT(x,y) ((x) |= (1<<(y)))
#define CLEARBIT(x,y) ((x) &= ~(1<<(y)))


uint8_t revbits2(uint8_t arg) { // ���������� ���������� ����  http://www.avrfreaks.net/index.php?name=PNphpBB2&file=printview&t=65618&start=0
  uint8_t result=0;
  if (arg & (1<<0)) result |= (0x80>>0);
  if (arg & (1<<1)) result |= (0x80>>1);
  if (arg & (1<<2)) result |= (0x80>>2);
  if (arg & (1<<3)) result |= (0x80>>3);
  if (arg & (1<<4)) result |= (0x80>>4);
  if (arg & (1<<5)) result |= (0x80>>5);
  if (arg & (1<<6)) result |= (0x80>>6);
  if (arg & (1<<7)) result |= (0x80>>7);
  return result;
}


// ������� ����� http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=292247
void Reg0_Out(uint8_t value) // 8-bit // ���������� �� �������� ���������!!!
{
  unsigned int i;
  for(i=8; i>0; i--)
  {
    if((value & (1<<(i-1))) != 0)
    {
      SETBIT(SHIFTREG_PORT,DAT0);
    }
    else
    {
      CLEARBIT(SHIFTREG_PORT,DAT0);
    }
    SETBIT(SHIFTREG_PORT,TACT0);
    CLEARBIT(SHIFTREG_PORT,DAT0);
    CLEARBIT(SHIFTREG_PORT,TACT0);
  }
  SETBIT(SHIFTREG_PORT,LOCK0);
  CLEARBIT(SHIFTREG_PORT,LOCK0);
}

void Reg1_Out(uint16_t value) //16-bit // �������� �� �������, ���� ��������� �������� ���������!!!
{
  unsigned int i;
  for(i=0; i<16; i++)
  {
    if((value & (1<<i)) != 0)
    {
      SETBIT(SHIFTREG_PORT,DAT1);
    }
    else
    {
      CLEARBIT(SHIFTREG_PORT,DAT1);
    }
    SETBIT(SHIFTREG_PORT,TACT1);
    CLEARBIT(SHIFTREG_PORT,DAT1);
    CLEARBIT(SHIFTREG_PORT,TACT1);
  }
  SETBIT(SHIFTREG_PORT,LOCK1);
  CLEARBIT(SHIFTREG_PORT,LOCK1);
}


// ���� ����� ����� http://avru.cwx.ru/uchebnik/7sigmentniki_led_and_avr.html
static uint8_t get_number_code_ca (unsigned int digit){  // common anode
switch (digit){
  case 0:return 0b00000011; //
    break;                     //
  case 1:return 0b10011111;
  break;
  case 2:return 0b00100101;
  break;
  case 3:return 0b00001101;
  break;
  case 4:return 0b10011001;
  break;
  case 5:return 0b01001001;
  break;
  case 6:return 0b01000001;
  break;
  case 7:return 0b00011111;
  break;
  case 8:return 0b00000001;
  break;
  case 9:return 0b00001001;
  break;

}
return 0;

}

static uint8_t get_number_code_cc (unsigned int digit){  // common catode
switch (digit){
  case 0:return 0b11111100; //�������� ���������� ���� Common Catode
    break;                     // ����� ������� ��������� �� ��������
  case 1:return 0b01100000;
  break;
  case 2:return 0b11011010;
  break;
  case 3:return 0b11110010;
  break;
  case 4:return 0b01100110;
  break;
  case 5:return 0b10110110;
  break;
  case 6:return 0b10111110;
  break;
  case 7:return 0b11100000;
  break;
  case 8:return 0b11111110;
  break;
  case 9:return 0b11110110;
  break;

}

}

int ShowTime(unsigned int  display2digits) { //���� 2-�������� ����� ������
uint16_t  LowerByteCode ;
uint16_t  HigherByteCode ;
uint16_t  seconds_display_register;

// 0b01001001,     //5
// 0b01000001,    //6

//     5        6
// 01001001
//            01000001
// 01001001   00000000  <= 01001001<<8

HigherByteCode=get_number_code_ca (display2digits/10);
LowerByteCode=get_number_code_ca (display2digits%10);
seconds_display_register=  (HigherByteCode<<8)|(LowerByteCode);
Reg1_Out(0);
Reg1_Out( seconds_display_register);
return 1;

}


#define T1_SW_PIN PIND0 // ������ �� ������ ������
#define T2_SW_PIN PIND1
#define T3_SW_PIN PIND2
#define T4_SW_PIN PIND3

#define C_NEW_ROUND_PIN PIND4  // ������ �� ����� ���������
#define C_ANSWER_PIN PIND5
#define C_CONTINUE_ROUND_PIN PIND6
#define C_PAUSE_PIN PIND7


/*

Function To Initialize TIMER0 In Fast
PWWM Mode.
http://extremeelectronics.co.in/avr-tutorials/sound-generation-by-avr-micro-tutorial-i/
*/



unsigned char NomBit (unsigned char ByteToAnalize){ // ��������� ���������� ����� ������������� ��� � ����

switch (ByteToAnalize)
{
  case 0: return 8;
  break;
  case 1: return 0;
  break;
  case 2: return 1;
  break;
  case 4: return 2;
  break;
  case 8: return 3;
  break;
  case 16: return 4;
  break;
  case 32: return 5;
  break;
  case 64: return 6;
  break;
  case 128: return 7;
  break;
  default: return 8;
  break;
}

}


/*uint8_t notes_c(uint8_t MelodyN){
    int i=0;
    while (Bip_notes[MelodyN][i]>0 ) {
       i++;
    }
    return i+1;

};*/
// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
int main (void)
{

  // === INITIALIZATION === INITIALIZATION  === INITIALIZATION  === INITIALIZATION  ===

  FGAME =  (1<<G_NEW_GAME);//����� ��� (1<<G_NEW_GAME)
  // G_NEW_GAME          0
  // 1	G_NEW_ROUND
  // 2	G_ANSWER
  // 3	G_CONTINUE_ROUND
  // 4	G_FALSESTART
  // 5	G_AFTER_TABLE_BUTT // ���� � ������ ���������
  FBUTT_TABLES = 0b00000000;// ���� ������ ������
  F_NACTIVE_TABLES = 0b00001111; // ��������� ������ ������

  unsigned char POINTS[4]={0,0,0,0}; // ���� ������
  unsigned char NO_ROUND = 0; // ����� ������
  unsigned char TABLES_FIRST_BUTTONS = 0b00000000; // ������ ������ ������� ��� ���������� ������
  unsigned char TABLES_OUT_OF_ROUND = 0b00000000; // ������ ������ ������, �� ������ � ��������� ������



  WDT_off();

  /*
  Atmega8A
  _______
  (RESET) PC6		   o|1	28|	PC5 (ADC5/SCL)  #define T2_R PORTC.5;
  (RXD) PD0		        |2	27|	PC4 (ADC4/SDA)  #define T2_G PORTC.4;
  (TXD) PD1		        |3	26|	PC3 (ADC3)      #define T2_SW PINC.3;
  (INT0) PD2		    |4	25|	PC2 (ADC2)      #define T1_R PORTC.2;
  (INT1) PD3		    |5	24|	PC1 (ADC1)      #define REG_DS PORTC1 //���� ��� ������ ������
  (XCK/T0) PD4		    |6	23|	PC0 (ADC0)      #define REG_SH_CP PINC0 //���� ��� �������� ���������
  VCC			       +|7	22|	GND
  GND			       -|8	21|	AREF
  (XTAL1/TOSC1) PB6	    |9	20|	AVCC
  (XTAL2/TOSC2) PB7     |10	19|	PB5 (SCK)
  (T1) PD5		        |11	18|	PB4 (MISO)
  (AIN0) PD6		    |12	17|	PB3 (MOSI/OC2)
  (AIN1) PD7		    |13	16|	PB2 (SS/OC1B)
  (ICP1) PB0		    |14	15|	PB1 (OC1A)
  -------
  */


  //DDRxy : ���������� ����� �����. 0 -- ����, 1 -- �����.
  DDRC =  0b00111111; //����� �� ������ 74HC595  ���0 = ���� 0,1,2; ���1 = ���� 3,4,5
  //  ��� DDRC = (1<<DDC0)|(1<<DDC3);

  //PORTxy : ���� ����� �������������� �� ����, �� 0 - Tri-state (Hi-Z); 1 -- ���� � ��������.
  PORTC = 0b00000000;
  //  ���
  //PORTC = (1<<PORTC0)|(1<<PORTC3);

  DDRD =  0b00000000; //���� �� ������
  //PORTxy : ���� ����� �������������� �� ����, �� 0 - Tri-state (Hi-Z); 1 -- ���� � ��������.
  PORTD = 0b11111111;



  uint32_t temp_sound_ticker=0;
  uint32_t temp_ticker=0;

  st_init(); // ����� sei() � timer.c

  uint8_t  clocks;
  const uint8_t round_time=61;
  clocks=round_time;


  uint32_t TI_SECUNDS_ticker = st_millis();
  uint32_t  TI_SECUNDS_period = 1000;

  uint32_t TI_SHOW_GAME_MODE_ticker = st_millis();
  uint32_t  TI_SHOW_GAME_MODE_period = 750;

  ShowTime(00);
  int i=0;

  int SMALL_P = 250;
  int BIG_P = 550;

  // WHILE ==  WHILE ==  WHILE ==  WHILE ==  WHILE ==  WHILE ==  WHILE ==  WHILE ==  WHILE ==
    //FTIMER |=(1<<TI_SHOW_GAME_MODE) ;
    FTIMER &= ~(1<<TI_SHOW_GAME_MODE) ;

unsigned char GAMEPAUSE; // �� �������� ������

 // ==== ==== == ������������ ����� =======

//DDRxy : ���������� ����� �����. 0 -- ����, 1 -- �����.
DDRB = (1<<PORTB0);
//PORTxy : ���� ����� �������������� �� ����, �� 0 - Tri-state (Hi-Z); 1 -- ���� � ��������.
//  PORTB = 0b00000000;
ctc_init();

OCR2 = 0; //0x47; // ������ ���������
//TIMSK &= ~_BV(OCIE2);
PORTB |= (1<<PORTB0 );


// ��������� ���
// ������ � flash http://microsin.ru/content/view/1070/44/
// {118,112,105,99,94,88,83,79,74,70,66,62,0},  // 7 12 Gamma
prog_uint8_t Bip_notes[11][12] =
                            {
                                {94,79,99,0},// ON    0
                                {62,62,0}, //1        1
                                {70,0}, //2           2
                                {79,0}, //3           3
                                {88,0}, //4          4
                                {99,0}, //5          5
                           {66,66,66,66,0},      // 6 Falsestart
                            {74,94,0},              // 7 Answer
                               {118,94,94,0},      // 8 NR
                                {99,83,0},          // 9 Table
                                {62,83,0}          // 10 Timer's On

                            };


uint32_t TI_SOUNDS_ticker = st_millis();
uint32_t  TI_SOUNDS_period = 500;
 uint8_t F_UPDATE_NOTE = 0;
 uint8_t note ;
 uint8_t notes ;
 uint8_t playBipN = 0;
  notes=12;
  note=notes;
//TIMSK |= _BV(OCIE2);
 #define  UPD_N 0

// ==== ==== ����� ������������ ����� =======


  while (1)
  {



// CHECK INPUT == CHECK INPUT == CHECK INPUT == CHECK INPUT == CHECK INPUT ==
    // ���� �������� �����
   // ������� ���� � ����� 547:
   // ������ G_CONTINUE_ROUND ���� C_CONTINUE_ROUND �� �������
                                                                                                        // �������� �� ENABLE_TABLE_CHECK
    if ( (~(PIND))&255 )
    {
        // COMMANDS BUTTONS

     if (GAMEPAUSE==1) // ���� ����� �� �� ������� �� ������ ������
        {
            F_INPUT &=~( 1<<ENABLE_TABLE_CHECK );
            FBUTT_TABLES = ((~(PIND))&255)&15 & (0) ; // �������� ����� ������� ��������� ����������
        }else {
            F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
            FBUTT_TABLES = ((~(PIND))&255)&15 & (~F_NACTIVE_TABLES ) ; // �������� ����� ������� ��������� ����������
        }


        if (  (FBUTT_TABLES) && ( F_INPUT & ( 1<<ENABLE_TABLE_CHECK )) )
        {
        	F_INPUT |= (1<<FI_TABLES_BUT);

             if (FGAME & (1<<G_ANSWER))
               {

                    /* ����� �� ���������
                        ShowTime(99); // GA
                        _delay_ms(SMALL_P);
                        ShowTime(99); // GA
                        _delay_ms(SMALL_P);
                        ShowTime(99); // GA
                        _delay_ms(SMALL_P);
                        ShowTime(FBUTT_TABLES);
                        _delay_ms(BIG_P);
                    */
                        FGAME |= (1<<G_FALSESTART);
                        FGAME &= ~(1<<G_ANSWER);

               } else {
                    playBipN=9 ; note=notes;
               }

               if (clocks>=round_time-1)  // ���� ��������� ������ ��� ������� 1 ��� -- ���������
                  {
            	       FGAME |= (1<<G_FALSESTART);
                       FGAME &= ~(1<<G_NEW_ROUND);
                  }
        }
        // END COMMANDS BUTTONS

        // CONTROL BUTTONS
        FBUTT_CONTROL = (((~(PIND))&255)&240); // �������� ������ �������
        if ( (FBUTT_CONTROL) && ( (st_millis()-temp_ticker)>300)  )
        {
           temp_ticker=st_millis(); // ��� ������������
           //  ShowTime(44);
           // _delay_ms(SMALL_P);
          //  ShowTime(FBUTT_CONTROL);
           // _delay_ms(BIG_P);

           F_INPUT |= (1<<FI_CONTROL_BUT);
            switch ( FBUTT_CONTROL )
              {
                case 16: //PIND4 // New Round
                    {   playBipN=8 ; note=notes;
                        FGAME = (1<<G_NEW_ROUND);
                        GAMEPAUSE=0;
                        TABLES_OUT_OF_ROUND =0;
                        F_INPUT |=( 1<<ENABLE_TABLE_CHECK );

                        clocks=round_time; // ���������� �������
                          FTIMER |=(1<<TI_SECUNDS ); // ������ ��������� ��������
                          //F_UPDATE_DISPLAY |= (1<<UPD_S);
                          F_INPUT |=( 1<<ENABLE_TABLE_CHECK );

                          if (NO_ROUND) // ���� �� ��� ������ ����� ���� ���������� ������ ������
                          {
                            int N_Command = NomBit(FBUTT_TABLES);
                            POINTS[N_Command]++; // ������ ���� ������, �� ��������� ������, � ��� �� �� ���� ������� ������ ������,
                                                    // �� ������� ���� �����.
                          }

                          F_NACTIVE_TABLES = 0; // ������� �� �����
                          TABLES_OUT_OF_ROUND = 0; // �� ������� ��������� ������ � ������ �����
                          TABLES_FIRST_BUTTONS = 0; // ��������� ��� ������ ��������
                          D_LEDS = 0; // ������ �� ����
                          F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����

                          FGAME = (1<<G_ROUND)|(1<<G_NEW_ROUND); // ���������� � ���� ���������� ���� ������� ������ ������
                          NO_ROUND++;

                    } // C_NEW_ROUND
                  break;
                case 32: //PIND5 // ANSWER
                    {
                        playBipN=7 ; note=notes;
                        if (FGAME & ( (1<<G_ROUND)|(1<<G_WAIT)) )
                        {
                          FGAME = (1<<G_ANSWER);
                          F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
                        }

                    } // C_ANSWER
                  break;
                case 64: //PIND6 Continue
                    {
                        playBipN=10 ; note=notes;
                        FGAME |= (1<<G_CONTINUE_ROUND);
                    } // C_CONTINUE_ROUND
                  break;
                case 128: //Pause
                    {
                        if (FTIMER & (1<<TI_SECUNDS ) )
                        {
                            FTIMER &= ~(1<<TI_SECUNDS);
                            GAMEPAUSE=1;
                        }else {
                            FTIMER |= (1<<TI_SECUNDS );
                            GAMEPAUSE=0;
                        }

                    } // RESERWED PAUSE ???
                  break;

                default:
                  break;
              }
        }
    }
    // END CONTROL BUTTONS

    // END CHECK INPUT == END CHECK INPUT == END CHECK INPUT == END CHECK INPUT ==
    // ʳ���� ����� �������� �����


    // GAME LOGICS == GAME LOGICS == GAME LOGICS == GAME LOGICS == GAME LOGICS ==
    // ���� ����� ���
    //���� 3 ���� � ������ ���������
    // ����=����� ������
    // Nbip=3
    // �������� ������ 10


    if ( (FGAME)&(1<<G_NEW_GAME) )
    {

      Reg0_Out(0b01010101);
      _delay_ms(100);
      Reg0_Out(~(0b01010101));
      _delay_ms(100);
      Reg0_Out(0b0);

     /* ShowTime(22);
        _delay_ms(1000);
        ShowTime(FTIMER &(1<<TI_SHOW_GAME_MODE) );
        _delay_ms(2000);
        //F_INPUT=0;
*/
      FTIMER &= ~(1<<TI_SECUNDS);

      FGAME = (1<<G_WAIT);//128;

    }

    if ( (FGAME) & (1<<G_WAIT) ) // �������� G_AFTER_NEW_GAME
    {
       //if (FTIMER &(1<<TI_SECUNDS ))
       //{
           // F_UPDATE_DISPLAY |= (1<<UPD_S); // ���������� ������ ����������
       //}

    }

  if ( (FGAME) & (1<<G_FALSESTART) )
    {
        playBipN=6 ; note=notes;
        FTIMER &= ~(1<<TI_SECUNDS ); // ������ ��������� ���������
        TABLES_FIRST_BUTTONS = FBUTT_TABLES; // ��� �������������
        TABLES_OUT_OF_ROUND |= TABLES_FIRST_BUTTONS; // ������ �� ������� �� �������� � ������
        F_NACTIVE_TABLES = 0b1111; // ���� �� �������� ������ ������� � �����
         F_INPUT &=~( 1<<ENABLE_TABLE_CHECK );

        D_LEDS = (TABLES_FIRST_BUTTONS <<4); // �������� �������� �������������
        F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����

        FGAME = (1<<G_WAIT); // ���������� �� ���������
    }


    if ( FGAME & ((1<<G_ROUND) |(1<<G_NEW_ROUND)|(1<<G_CONTINUE_ROUND)) ) // ���� ������, ���������� ������� ������ ���� ������ ������ ������,
                                          // ���  ���� ������.
                                          // ��� �� ���� ����� ������ ����� (����� ������ �� ��������)
                                          // ����� ����� G_AFTER_NEW_ROUND
    {
        if ( FGAME&(1<<G_NEW_ROUND))
        {
            TABLES_OUT_OF_ROUND = 0;
            F_NACTIVE_TABLES = TABLES_OUT_OF_ROUND ; // ������ ������� � �����, �� �� ������
            //TABLES_FIRST_BUTTONS = 0;
            D_LEDS = ( (TABLES_OUT_OF_ROUND) <<4); // �������� �������� ��� ��� �� ����������
            F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����
            FTIMER |= (1<<TI_SECUNDS ); // ������ ��������� ��������
            FGAME = (1<<G_ROUND);
            F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
        }

        if ( FGAME&(1<<G_CONTINUE_ROUND))
        {

            TABLES_OUT_OF_ROUND |= TABLES_FIRST_BUTTONS; // ������ �� ������� �� �������� � ������
            F_NACTIVE_TABLES = TABLES_OUT_OF_ROUND ; // ������ ������� � �����, �� �� ������
            //TABLES_FIRST_BUTTONS = 0;
            D_LEDS = ((TABLES_OUT_OF_ROUND)<<4); // �������� �������� ��� ��� �� ����������
            F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����
            FTIMER |= (1<<TI_SECUNDS ); // ������ ��������� ��������
            FGAME = (1<<G_ROUND);
            F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
        }

        //if ( ( F_INPUT&((1<<FI_TABLES_BUT)  ) )  &&  !( (FGAME) &(1<<G_WAIT)) ) // ����� � ������ ��������
        if ( ( FBUTT_TABLES )  &&  !( (FGAME) &(1<<G_WAIT)) ) // ����� � ������ ��������
        {
            TABLES_FIRST_BUTTONS = FBUTT_TABLES;
            F_NACTIVE_TABLES = 0b1111; // ��������� ����� ������
            FTIMER &= ~(1<<TI_SECUNDS ); // ������������ ���������
            D_LEDS =   TABLES_FIRST_BUTTONS |  ((~TABLES_FIRST_BUTTONS)<<4) ; // �������� ��� �� ����� � ��� �����
            F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����
            F_INPUT &=~( 1<<ENABLE_TABLE_CHECK );
            FGAME = (1<<G_WAIT)|(1<<G_ROUND); // ���������� �� ���������

            // ������� ������� ���������
        }
        else // ������� �� �� ���������
        {

           // F_UPDATE_DISPLAY |= (1<<UPD_S); // ���������� ������ ����������
        }

    }



   if ( (FGAME) & (1<<G_ANSWER)) // ����� ������. ��������� �����. ������ ������� �������.
    {

        TABLES_OUT_OF_ROUND |= TABLES_FIRST_BUTTONS; // �������� ������ ����������� � ������ �������� � ��������� ������ �� ������� ������ ������
        FTIMER &= ~(1<<TI_SECUNDS ); // ������ ��������� ���������
        F_NACTIVE_TABLES = TABLES_OUT_OF_ROUND; //������� ��� ������� �������� �� �� ������
        D_LEDS = 0;//(TABLES_OUT_OF_ROUND<<4) ; // �������� ��� ��� �����
        F_UPDATE_DISPLAY |= (1<<UPD_L); // ������� ������ ����
        FGAME = (1<<G_WAIT)|(1<<G_ANSWER); // ���������� �� ���������
         F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
    }



    if (FGAME & (1<<G_CONTINUE_ROUND) )
    {
        TABLES_OUT_OF_ROUND |= TABLES_FIRST_BUTTONS; // ������ �� ������� �� �������� � ������
         F_INPUT |=( 1<<ENABLE_TABLE_CHECK );
        FGAME = (1<<G_ROUND)|(1<<G_CONTINUE_ROUND);

    }

    if (FGAME == (1<<G_NEW_ROUND)) // ������� �� ����� ��������� ���� ��������� �������
    {

    }


    // END GAME LOGICS == END GAME LOGICS == END GAME LOGICS == END GAME LOGICS ==


    // DISPLAY == DISPLAY == DISPLAY == DISPLAY == DISPLAY == DISPLAY == DISPLAY ==
    // ���� �����������

      if ( F_UPDATE_DISPLAY & (1<<UPD_L) )
      {
        // LEDs ������
        //unsigned char __builtin_avr_swap (unsigned char)
        Reg0_Out(D_LEDS);
        F_UPDATE_DISPLAY &= ~(1<<UPD_L);
      }

      if ( F_UPDATE_DISPLAY & (1<<UPD_S) )
      {

        ShowTime(clocks);
        if ( (clocks<6) && (clocks>0) )
        {
           playBipN=clocks ; note=notes;
        }
        //_delay_ms(100);
        F_UPDATE_DISPLAY &= ~(1<<UPD_S);
      }

       if ( F_UPDATE_DISPLAY & (1<<UPD_G) ) // �������� �����, ���� �������� UPD_G
      {

       ShowTime(11);
        _delay_ms(SMALL_P);
        ShowTime(FGAME);
        _delay_ms(SMALL_P);
        ShowTime(22);
        _delay_ms(SMALL_P);
        ShowTime(FTIMER );
        _delay_ms(SMALL_P);

          ShowTime(33);
        _delay_ms(SMALL_P);
        ShowTime(FBUTT_TABLES);
        _delay_ms(SMALL_P);

         ShowTime(55);
        _delay_ms(SMALL_P);
        ShowTime(F_NACTIVE_TABLES);
        _delay_ms(BIG_P);

        ShowTime(66);
        _delay_ms(SMALL_P);
        ShowTime(F_INPUT);
        _delay_ms(BIG_P);


        ShowTime(88); // GA
            _delay_ms(SMALL_P);
            ShowTime(TABLES_OUT_OF_ROUND);
            _delay_ms(BIG_P);

        F_UPDATE_DISPLAY &= ~(1<<UPD_G);


      }


    // END DISPLAY == END DISPLAY == END DISPLAY == END DISPLAY == END DISPLAY ==

    // TIMERS == TIMERS == TIMERS == TIMERS == TIMERS == TIMERS == TIMERS == TIMERS ==

      if (FTIMER & (1<<TI_SECUNDS ) )
      {
        if ( (st_millis()-  TI_SECUNDS_ticker) > TI_SECUNDS_period)
          {
           clocks--;
           if (!clocks)
           {
             FTIMER &= ~(1<<TI_SECUNDS);
           }
           TI_SECUNDS_ticker=st_millis();
           F_UPDATE_DISPLAY |= (1<<UPD_S);
           }
      }

      if (FTIMER & (1<<TI_SHOW_GAME_MODE  ) )
      {
        if ( (st_millis() -  TI_SHOW_GAME_MODE_ticker) > TI_SHOW_GAME_MODE_period)
          {
           TI_SHOW_GAME_MODE_ticker=st_millis();
           F_UPDATE_DISPLAY |= (1<<UPD_G);
           }
      }

      if (note==12) { TIMSK |= _BV(OCIE2);  };
      if (note==0)  { TIMSK &= ~_BV(OCIE2); };

      if ((note) && ( (st_millis() -  TI_SOUNDS_ticker ) > TI_SOUNDS_period ) )
          {
           TI_SOUNDS_ticker =st_millis();
            OCR2 = Bip_notes[playBipN][notes-note];
           note--;
           if (OCR2==0){ note=0;};
          }



    // END TIMERS == END TIMERS == END TIMERS == END TIMERS == END TIMERS == END TIMERS ==

  } // END WHILE == END WHILE == END WHILE == END WHILE == END WHILE == END WHILE == END WHILE ==

  return 1;

}
