/*
Code from Arduino's \hardware\arduino\cores\arduino\wiring.c
by Arduino team (http://arduino.cc)
*/


#include <avr/interrupt.h>
#include "timer.h"

volatile static uint32_t st_timer0_millis;
static uint8_t st_timer0_fract;

ISR(TIMER0_OVF_vect)
{
  // copy these to local variables so they can be stored in registers
  // (volatile variables must be read from memory on every access)
  uint32_t m = st_timer0_millis;
  uint8_t f = st_timer0_fract;

  m += ST_MILLIS_INC;
  f += ST_FRACT_INC;
  if (f >= ST_FRACT_MAX) {
    f -= ST_FRACT_MAX;
    m += 1;
  }

  st_timer0_fract = f;
  st_timer0_millis = m;
}

uint32_t st_millis(void)
{
  uint32_t m;

  uint8_t oldSREG = SREG;

  // disable interrupts while we read st_timer0_millis or we might get an
  // inconsistent value (e.g. in the middle of a write to st_timer0_millis)
  cli();
  m = st_timer0_millis;

  SREG = oldSREG; // restore sei() if it was enabled

  return m;
}

void st_init(void)
{
  // Set prescaler to 64
  TCCR0 |= (_BV(CS01) | _BV(CS00));

  // Enable timer 0 overflow interrupt
  TIMSK |= _BV(TOIE0);
  sei();
}
