/*
System tick timer

Code from Arduino's \hardware\arduino\cores\arduino\wiring.c
by Arduino team (http://arduino.cc)

WARNING: 
- Timer configuration for ATmega8A only!
- millis() value will rollover each 4294967295 milliseconds (about 49 days),
one can add rollover count as here: http://www.faludi.com/2007/12/18/arduino-millis-rollover-handling/
*/

#ifndef ST_H
#define ST_H

#define ST_CLOCK_CYCLES_TO_MICROSECONDS(a) ( ((a) * 1000L) / (F_CPU / 1000L) )

// the prescaler is set so that timer0 ticks every 64 clock cycles, and the
// the overflow handler is called every 256 ticks.
#define ST_MICROSECONDS_PER_TIMER0_OVERFLOW (ST_CLOCK_CYCLES_TO_MICROSECONDS(64 * 256))

// the whole number of milliseconds per timer0 overflow
#define ST_MILLIS_INC (ST_MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)

// the fractional number of milliseconds per timer0 overflow. we shift right
// by three to fit these numbers into a byte. (for the clock speeds we care
// about - 8 and 16 MHz - this doesn't lose precision.)
#define ST_FRACT_INC ((ST_MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define ST_FRACT_MAX (1000 >> 3)

uint32_t st_millis(void);
void st_init(void);

#endif	// ST_H
