Печатные платы

Плата кнопок пульта
control_mirror_negative.png	 - негатив зеркальная
control_pads.png - расположение контактных площадок, расположение элементов
control_pads.svg - то же, векторный файл в формате Inkscape SVG.

Главная плата
main_mirror_negative.png
main_pads.png
main_pads.svg
main_V2.png - компактный вариант (исходиники в формате TopoR затерялись)

Плата кнопки на столе команды
table_mirror_negative.png
table_pads.jpg

Плата индикатора таймера
timer_mirror_negative.png
timer_pads.png
timer_pads.svg